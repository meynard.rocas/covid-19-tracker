import { useState, useRef, useEffect } from 'react'
import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY

const GlobalMap = ({data}) => {

	const mapContainerRef = useRef(null)

	const [latitude, setLatitude] = useState(0)
	const [longitude, setLongitude] = useState(0)
	const [zoom, setZoom] = useState(0) 

	useEffect(()=> {

		fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${data.country_name}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setLongitude(data.features[0].center[0])
			setLatitude(data.features[0].center[1])
			setZoom(1)

		})

		// Instantiate a new mapbox map object
		const map = new mapboxgl.Map({
			// set the container for the map as the current component
			container: mapContainerRef.current,
			// set the style for the map
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [longitude, latitude],
			zoom: zoom
		})

		// add navigation control or zoom in/out
		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

		// create a marker centered on the provided longitude and latitude
		const marker = new mapboxgl.Marker()
		.setLngLat([longitude, latitude])
		.addTo(map)

		// clean up and release resources associated with the map
		return () => map.remove()
	}, [latitude, longitude])

	return <div className="mapContainer" ref={mapContainerRef} />

}

export default GlobalMap
