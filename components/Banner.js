import { Jumbotron, Row, Card, Col, CardDeck, Container } from 'react-bootstrap'
import Button from 'react-bootstrap/Button'
import Link from 'next/link'
import GlobalMap from '../components/GlobalMap';

export default function Banner({ country, new_cases, new_deaths, recoveries, criticals }) {
    return (

        <React.Fragment>

            <CardDeck>
                <Card className="shadow-lg">
                    <Row className="m-3">
                        <Card className="text-center shadow-lg">
                            <h2>{country}</h2>
                        </Card>
                    </Row>

                    <Row className="m-3">
                        <Card className="shadow-lg text-center">
                            <h6 className="m-3">new confirmed cases: {new_cases}</h6>
                        </Card>
                        <Card className="shadow-lg text-center">
                            <h6 className="m-3">critical: {criticals}</h6>
                        </Card>
                    </Row>

                    <Row className="m-3">
                        <Card className="shadow-lg text-center">
                            <h6 className="m-3">today's deaths: {new_deaths}</h6>
                        </Card>
                        <Card className="shadow-lg text-center">
                            <h6 className="m-3">total recoveries: {recoveries}</h6>
                        </Card>
                    </Row>

                </Card>
            </CardDeck>    
           

        </React.Fragment>
    )
}