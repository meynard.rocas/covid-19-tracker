import { Doughnut, Pie, Line, Bar } from 'react-chartjs-2';
import moment from 'moment'
import { useState, useEffect } from 'react'


export default function DoughnutChart({ criticals, deaths, recoveries, active_cases }) {
    
	return (

		<React.Fragment>

			<Pie
				data = {
					{
						datasets: [{
							data: [criticals, deaths, recoveries],
							backgroundColor: ["purple", "red", "lightblue"]
						}],
						labels: [
							'Criticals',
							'Deaths',
							'Recoveries'
						]
					}
				} redraw={ false }
			/>

			<Bar
				data = {
					{
						datasets: [{
							data: [active_cases, recoveries, deaths],
							backgroundColor: ["green", "lightblue", "red"]
						}],
						labels: [
							'Active Cases',
							'Recoveries',
							'Deaths'
						]
					}
				} redraw={ false }
			/>

		</React.Fragment>
	)
}