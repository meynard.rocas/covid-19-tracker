import toNum from '../../helpers/toNum'
import { Doughnut } from 'react-chartjs-2'
import { useRef, useEffect } from 'react'
import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY
import { Form, Button, Col, Row, Container, Card } from 'react-bootstrap';

export default function top({data}) {

	console.log(data)

	const countriesStats = data.countries_stat
	const mapContainerRef = useRef(null)

	console.log(countriesStats)

	const countriesCases = countriesStats.map(countryStat => {
		return {
            name: countryStat.country_name,
            cases: toNum(countryStat.cases)
        }
	})

	console.log(countriesCases)

	//   a        b
	// [USA, Philippines, China]

	countriesCases.sort((a,b) => {
		// if statement checks if index a.cases is less than b.cases
		if (a.cases < b.cases) {
			// return 1 means that if statement is true then this put element a to the location of b on the array which means on the right
			return 1
		} else if (a.cases > b.cases) {
			// return -1 means to moves element a to the left
			return -1
		} else {
			return 0
		}
	})

	const countriesArray = countriesCases.sort((a,b) => {
		// if statement checks if index a.cases is less than b.cases
		if (a.cases < b.cases) {
			// return 1 means that if statement is true then this put element a to the location of b on the array which means on the right
			return 1
		} else if (a.cases > b.cases) {
			// return -1 means to moves element a to the left
			return -1
		} else {
			return 0
		}
	}).slice(0,10)

	console.log(countriesArray)

	let countryNames = [];

	countriesArray.forEach(country => {
		return countryNames.push(country.name)
	})

	console.log(countryNames)

	let countryCases = [];

	countriesArray.forEach(country => {
		return countryCases.push(country.cases)
	})

	console.log(countryCases)

	useEffect(()=> {

		// Instantiate a new mapbox map object
		const map = new mapboxgl.Map({
			// set the container for the map as the current component
			container: mapContainerRef.current,
			// set the style for the map
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [44.63, 28.77],
			zoom: 0
		})

		// add navigation control or zoom in/out
		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

		for(let counter=0; counter < 10; counter++) {

		fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${countryNames[counter]}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
		.then(res => res.json())
		.then(data => {
				console.log(data)

				const marker = new mapboxgl.Marker()
				.setLngLat([data.features[0].center[0], data.features[0].center[1]])
				.addTo(map)

			})
		}
		// clean up and release resources associated with the map
		return () => map.remove()
	}, [])

	return (
		<React.Fragment>

			<Card>

				<Card>
					<h1>10 Countries With The Highest - Number of Cases</h1>
				</Card>

				<Card>
					<Doughnut data={{
					// 	datasets: [{
					// 		data: [countriesCases[0].cases, countriesCases[1].cases, countriesCases[2].cases, countriesCases[3].cases, countriesCases[4].cases,countriesCases[5].cases,countriesCases[6].cases,countriesCases[7].cases,countriesCases[8].cases,countriesCases[9].cases],
					// 		backgroundColor: ["red", "green","black","yellow","green","blue","indigo","violet","orange","white","gray"]
					// 	}],
					// 	labels: [countriesCases[0].name, countriesCases[1].name, countriesCases[2].name, countriesCases[3].name, countriesCases[4].name,countriesCases[5].name,countriesCases[6].name,countriesCases[7].name,countriesCases[8].name,countriesCases[9].name]
					// }} redraw={false} />

						datasets: [{
							data: [...countryCases],
							backgroundColor: ["red", "green","black","yellow","green","blue","indigo","violet","orange","white","gray"]
						}],
						labels: [...countryNames]
					}} redraw={false} />
				</Card>
			
				<div className="mapContainer" ref={mapContainerRef} />

			</Card>

		</React.Fragment>
	)
}

export async function getStaticProps() {
	//fetch data from the /courses API endpoint    
	const res = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php', {
		method: 'GET',
		headers: {
		'x-rapidapi-host': 'coronavirus-monitor.p.rapidapi.com',
		'x-rapidapi-key': '6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539'
		}
	})
	const data = await res.json()

	//return the props
	return { 
		props: {
			data
		}
	}
}