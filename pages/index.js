import Head from 'next/head';

import { useState, useRef, useEffect } from 'react'
import { Form, Button, Col, Row, Container, Card } from 'react-bootstrap';

import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY

import toNum from '../helpers/toNum'

import DoughnutChart from '../components/DoughnutChart'
import Banner from '../components/Banner';
import GlobalMap from '../components/GlobalMap';

export default function Home({ data }) {

  const countriesStats = data.countries_stat

  const [targetCountry, setTargetCountry] = useState('')

  const [country, setCountry] = useState('')

  const [newCases, setNewCases] = useState(0)
  const [newDeaths, setNewDeaths] = useState(0)
  const [activeCases, setActiveCases] = useState(0)
  const [criticals, setCriticals] = useState(0)
  const [deaths, setDeaths] = useState(0)
  const [recoveries, setRecoveries] = useState(0)

  const [latitude, setLatitude] = useState(0)
  const [longitude, setLongitude] = useState(0)
  const [zoom, setZoom] = useState(0)

  // users the useRef() hook to set a container where the map will be rendered
  const mapContainerRef = useRef(null)

  function search(e) {
    e.preventDefault()

    const match = countriesStats.find(country => country.country_name.toLowerCase() === targetCountry.toLowerCase())

    if(match) {

      setCountry(match.country_name)
      setNewCases(toNum(match.new_cases))
      setNewDeaths(toNum(match.new_deaths))
      setActiveCases(toNum(match.active_cases))
      setCriticals(toNum(match.serious_critical))
      setDeaths(toNum(match.deaths))
      setRecoveries(toNum(match.total_recovered))

      console.log(data.countries_stat)
      console.log(match)

      // use mapbox's geocoding API to return coordinates
      fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${targetCountry}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
      .then(res => res.json())
      .then(data => {

        setLongitude(data.features[0].center[0])
        setLatitude(data.features[0].center[1])
        setZoom(1)
      })
    }

  }

  useEffect(()=> {
    // Instantiate a new mapbox map object
    const map = new mapboxgl.Map({
      // set the container for the map as the current component
      container: mapContainerRef.current,
      // set the style for the map
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [longitude, latitude],
      zoom: zoom
    })

    // add navigation control or zoom in/out
    map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

    // create a marker centered on the provided longitude and latitude
    const marker = new mapboxgl.Marker()
    .setLngLat([longitude, latitude])
    .addTo(map)

    // clean up and release resources associated with the map
    return () => map.remove()
  }, [latitude, longitude])

  return (
    <React.Fragment>

      <Head>
        <title>Covid-19 Tracker</title>
      </Head>
      
      <Form onSubmit={e => search(e)} className="ml-5 mr-5">
        <Form.Group controlId="country">

          <p className="mt-3 text-center">Enter the Country</p>

          <Form.Control 
            type="text"
            value={targetCountry} 
            onChange={e => setTargetCountry(e.target.value)}
          />

          <Button variant="danger" type="submit" block className="mt-3">
            SEARCH
          </Button>

        </Form.Group>
      </Form>

      { (country.toLowerCase() === targetCountry.toLowerCase())
      ?
        <Row>
          <Col>
            <Banner
              new_cases={newCases}
              new_deaths={newDeaths}
              country={country}
              deaths={deaths}
              criticals={criticals}
              recoveries={recoveries}
            />
            <div className="mapContainer" ref={mapContainerRef} />
          </Col>
          <Col>
            <Card className="text-center shadow-lg">
              <DoughnutChart
                active_cases={activeCases}
                criticals={criticals}
                deaths={deaths}
                recoveries={recoveries}
              />
            </Card>
          </Col>
        </Row>
      : 
        <h1></h1>
      }

    </React.Fragment>
  )
}

export async function getStaticProps({params}) {
  const res = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php', {
    method: 'GET',
    headers: {
    'x-rapidapi-host': 'coronavirus-monitor.p.rapidapi.com',
    'x-rapidapi-key': '6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539'
    }
  })
  const data = await res.json()

  return {
    props: {
      data
    }
  }
}